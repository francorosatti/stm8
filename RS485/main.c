/****************************************************
* Name: main.c (RS458Test)
* Desc: Testeo de la comunicación via RS485
* Vers: 0.1
* Date: Aug-2017
* Auth: Franco Rosatti
****************************************************/

#include <iostm8s003k3.h>

#define SEGUNDOS   1000
#define LED        PD_ODR_bit.ODR0
#define RE         PD_ODR_bit.ODR2
#define DE         PD_ODR_bit.ODR3

unsigned char data = 0;
unsigned char newData = 0;
unsigned int segundos = SEGUNDOS;

void delay(unsigned long n){
    while (n-- > 0);
}

void CLK_Init(){
  while(CLK_ICKR_bit.HSIRDY != 1){};
  CLK_CKDIVR = 0x18;
}

void Timer_Init(){
  TIM2_PSCR = 0x00; //Prescaler en 1;
  /**************************************************
    ARR = ______fmaster_______
            PSCR * ftimebase
    fmaster = 2MHz
    ftimebase = 1kHz
    PSCR = 1
  
    ARR = 2000dec (7D0h)
  ***************************************************/
  TIM2_ARRH = 0x07; //Auto-reload Register High
  TIM2_ARRL = 0xD0; //Auto-reload Register Low
  TIM2_IER = 0x01; //Habilito la interrupcion por Update
  TIM2_CR1 = 0x01; //Habilito el timer
}

void GPIO_Init(){
  // LED
  PD_DDR_bit.DDR0 = 1; //Output
  PD_CR1_bit.C10 = 1; //Push-pull
  PD_CR2_bit.C20 = 1; //High Speed
  LED = 1; //Output value
  //RE
  PD_DDR_bit.DDR2 = 1; //Output
  PD_CR1_bit.C12 = 1; //Push-pull
  PD_CR2_bit.C22 = 1; //High Speed
  //DE
  PD_DDR_bit.DDR3 = 1; //Output
  PD_CR1_bit.C13 = 1; //Push-pull
  PD_CR2_bit.C23 = 1; //High Speed  
}

void UART1_Init(){
  UART1_BRR2 = 0x00;
  UART1_BRR1 = 0x0D;
  UART1_CR1 = 0x00;
  UART1_CR3 = 0x00;
  UART1_CR4 = 0x00;
  UART1_CR5 = 0x00;
  UART1_GTR = 0x00;
  UART1_PSCR = 0x00;
  UART1_CR2 = 0x6C;
  RE = 0;
  DE = 0;
}

#pragma vector=UART1_R_RXNE_vector
__interrupt void UartReceiveHandler(void){
  unsigned char sts = 0;
  sts = UART1_SR;
  if(sts & MASK_UART1_SR_RXNE){
    data = UART1_DR;
    newData = 1;
  }
}

#pragma vector=UART1_T_TC_vector
__interrupt void UartTransmissionCompleteHandler(void){
  unsigned char sts = 0;
  sts = UART1_SR;
  if(sts & MASK_UART1_SR_TC){
    RE = 0;
    DE = 0;
    UART1_SR &= (~MASK_UART1_SR_TC);
  }
}

#pragma vector=TIM2_OVR_UIF_vector
__interrupt void Timer2OverflowHandler(void){
  TIM2_SR1 &= (~MASK_TIM2_SR1_UIF);
  //Aqui entra cada 1ms
  segundos--;
  if(!segundos){
    //Aqui entra cada 1s
    segundos = SEGUNDOS;
    LED = !LED;
  }
}

int main(void){
  CLK_Init();
  Timer_Init();
  GPIO_Init();
  UART1_Init();
  asm("rim"); //Global Interrupt Enable
  while (1){
    if(newData){
      newData = 0;
      delay(10);
      RE = 1;
      DE = 1;
      delay(10);
      UART1_DR = data;
    }
  }
}