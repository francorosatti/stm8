/****************************************************
* Name: main.c (TemperatureSensor)
* Desc: Testeo del SPI con el sensor de temperatura
* Vers: 0.1
* Date: Jun-2017
* Auth: Franco Rosatti
****************************************************/

#include <iostm8s003k3.h>

//
// Delay loop
//
// Actual delay depends on clock settings
// and compiler optimization settings.
//

#define CS PB_ODR_bit.ODR7
#define LED PD_ODR_bit.ODR0

void delay(unsigned int n){
    while (n-- > 0);
}

void GPIO_Init(){
  // LED
  PD_DDR_bit.DDR0 = 1; //Output
  PD_CR1_bit.C10 = 1; //Push-pull
  PD_CR2_bit.C20 = 1; //High Speed
  LED = 1; //Output value
  // CS
  PB_DDR_bit.DDR7 = 1; //Output
  PB_CR1_bit.C17 = 1; //Push-pull
  PB_CR2_bit.C27 = 0; //Slow Speed
  CS = 1; //High Level
  //SPI output pins must be set-up as push-pull, fast slope for optimal operation.
  //MOSI
  PC_DDR_bit.DDR6 = 1; //Output
  PC_CR1_bit.C16 = 1; //Push-pull
  PC_CR2_bit.C26 = 1; //High Speed
  //SCK
  PC_DDR_bit.DDR5 = 1; //Output
  PC_CR1_bit.C15 = 1; //Push-pull
  PC_CR2_bit.C25 = 1; //High Speed 
}

void SPI_Init(){
  CLK_PCKENR1 |= 0x2; //Enable SPI peripheral clock
  SPI_CR1 = (unsigned char)0x3D; //MSB First,BaudRate: fmaster/256 Slowest,SCK Idle Low, Falling Edge, Master Mode
  SPI_CR2 = (unsigned char)0x07; //Input, managed by software, RXOnly,CRC Disabled
  delay(1000);
}

/*#pragma vector = SPI_TXE_vector
__interrupt void SPI_TXE_IRQHandler(void){
  //Do something
}*/

unsigned char SPI_Recv(){
  unsigned int ret1 = 0;
  unsigned int ret2 = 0;
  CS = 0x00; //Reset CS
  SPI_CR1_SPE = 0x01; //SPI Enable
  while(SPI_SR_RXNE == 0){} //Wait for RxFlag
  ret1 = (unsigned int)SPI_DR; //Read RX Buffer
  while(SPI_SR_RXNE == 0){} //Wait for RxFlag
  ret2 = (unsigned int)SPI_DR; //Read RX Buffer
  CS = 0x01;//Set CS
  SPI_CR1_SPE = 0x00; //SPI Disable
  ret1 = ((ret1<<8)+ret2)>>2; //Return read value
  //temp = (float)(0.03125 * ret1);
  return ret1;
}

int main( void ){
  GPIO_Init();
  SPI_Init();
  while (1){
    if(SPI_Recv() != 0){
      //Toogle Led 
      LED = !LED;
    }
    delay(60000);
  }
}