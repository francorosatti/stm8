/****************************************************
* Name: main.c (ADCTest)
* Desc: Testeo del ADC
* Vers: 0.1
* Date: Aug-2017
* Auth: Franco Rosatti
****************************************************/

#include <iostm8s003k3.h>

#define LED             PD_ODR_bit.ODR0

unsigned int data = 0;
unsigned int milisegundos = 0;
unsigned int adc_ref = 0;

void delay(unsigned long n){
    while (n-- > 0);
}

void CLK_Init(){
  while(CLK_ICKR_bit.HSIRDY != 1){};
  CLK_CKDIVR = 0x18;
}

void Timer_Init(){
  TIM2_PSCR = 0x00; //Prescaler en 1;
  /**************************************************
    ARR = ______fmaster_______
            PSCR * ftimebase
    fmaster = 2MHz
    ftimebase = 1kHz
    PSCR = 1
  
    ARR = 2000dec (7D0h)
  ***************************************************/
  TIM2_ARRH = 0x07; //Auto-reload Register High
  TIM2_ARRL = 0xD0; //Auto-reload Register Low
  TIM2_IER = 0x01; //Habilito la interrupcion por Update
  TIM2_CR1 = 0x01; //Habilito el timer
}

void GPIO_Init(){
  // LED
  PD_DDR_bit.DDR0 = 1; //Output
  PD_CR1_bit.C10 = 1; //Push-pull
  PD_CR2_bit.C20 = 1; //High Speed
  LED = 1; //Output value
}

void ADC_Init(){
  ADC_CSR = MASK_ADC_CSR_EOCIE; //EOC interrupt enabled
  ADC_CR2 = MASK_ADC_CR2_ALIGN; //Align = right
  ADC_CR3 = 0x00;
  ADC_CR1 = MASK_ADC_CR1_ADON; //Enciendo el ADC
}

void ADC_Off(){
  ADC_CR1 &= !(~MASK_ADC_CR1_ADON); //Enciendo el ADC
}

void ADC_On(){
  ADC_CR1 |= MASK_ADC_CR1_ADON; //Enciendo el ADC
}

void ADC_StartConvertion(){
  //El bit ADC_CR1_ADON ya debe estar seteado de cuando se encendi� el ADC
  ADC_CR1 |= MASK_ADC_CR1_ADON;
}

#pragma vector=ADC1_EOC_vector
__interrupt void ADCEndOfConversionHandler(void){
  /***************************************************
  * Como ADC_CR2_ALIGN = RIGHT debo leer primero
  * el ADC_DRL y luego el ADC_DRH, este ultimo
  * contiene en sus dos bit menos significativos
  * los dos bits mas significativos de la conversion
  ****************************************************/
  ADC_CSR &= (~MASK_ADC_CSR_EOC);
  data = ADC_DRL;
  data |= (((unsigned int)ADC_DRH)<<8);
}

#pragma vector=TIM2_OVR_UIF_vector
__interrupt void Timer2OverflowHandler(void){
  TIM2_SR1 &= (~MASK_TIM2_SR1_UIF);
  //Aqui entra cada 1ms
  milisegundos++;
}

int main(void)
{
  CLK_Init();
  Timer_Init();
  GPIO_Init();
  ADC_Init();
  asm("rim"); //Global Interrupt Enable
  while (1){
    if(milisegundos - adc_ref >= 5000){
      adc_ref += 5000;
      ADC_StartConvertion();
      LED = !LED;
    }
  }
}
