/********************************************************
* Name: main.c (ModbusTest)
* Desc: Testeo del protocolo Modbus
* Auth: Franco Rosatti
* Vers: 0.1
* Date: Aug-2017
********************************************************/

#include "system.h"
#include "clock.h"
#include "wdt.h"
#include "timer.h"
#include "ports.h"
#include "uart.h"
#include "spi.h"
#include "adc.h"
#include "iostm8s003k3.h"

#define LED_TGL_TIMEOUT         1000
#define TEMP_MED_TIMEOUT       250
#define ADC_MED_TIMEOUT       50

static unsigned int led_tickref;
static unsigned int spi_tickref;
static unsigned int adc_tickref;
unsigned int Temperatura = 0;
extern unsigned int ADC_Data;
extern unsigned int ADC_Buf[20];

int main(void){
  int i = 0;
  int acc = 0;
  Clock_Init();
  Timer_Init();
  Ports_Init();
  Uart_Init();
  SPI_Init();
  ADC_Init();
  //Wdt_Init();
  led_tickref = Timer_GetTickCount();
  spi_tickref = Timer_GetTickCount();
  adc_tickref = Timer_GetTickCount();
  ADC_ON;
  LED_OFF;
  IRQ_ENABLED;
  while (1){
    //Reseteo el WatchDog Timer
    //Wdt_Reset();
    
    //Toogle Led
    if(Timer_GetTickCount() - led_tickref >= LED_TGL_TIMEOUT){
      led_tickref += LED_TGL_TIMEOUT;
      LED_TGL;
    }

    //Medicion de Temperatura
    if(Timer_GetTickCount() - spi_tickref >= TEMP_MED_TIMEOUT){
      spi_tickref += TEMP_MED_TIMEOUT;
      Temperatura = SPI_Recv();
    }

    //Medicion del ADC
    if(Timer_GetTickCount() - adc_tickref >= ADC_MED_TIMEOUT){
      adc_tickref += ADC_MED_TIMEOUT;
      ADC_StartConvertion();
    }
    
    //Proceso del puerto serie
    Uart_Motor();
    
    acc = 0;
    for(i = 0; i < 20; i++){
      acc += ADC_Buf[i];
    }
    acc /= i;
    ADC_Data = acc; 
  }
}