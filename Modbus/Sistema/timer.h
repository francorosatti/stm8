/**********************************************************************
* Name: timer.h
* Desc: Header de la libreria timer.c
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Aug-2017
**********************************************************************/

#ifndef TIMER_H
#define TIMER_H

#define TICKS_POR_SEGUNDO 1000

//*********************************************************************
// Timer_Init: Inicializa el Timer 2
//*********************************************************************
void Timer_Init(void);

//*********************************************************************
// Timer_GetTickCount: Obtiene la cuenta actual del Timer 2 en ms
//*********************************************************************
unsigned int Timer_GetTickCount(void);

//*********************************************************************
// Timer_GetSecdCount: Obtiene la cuenta actual del Timer 2 en segundos
//*********************************************************************
unsigned int Timer_GetSecdCount(void);

//*********************************************************************
// Timer_GetCounter: Obtiene el valor del contador de hardware
//*********************************************************************
unsigned short Timer_GetCounter(void);

//*********************************************************************
// Timer_Sleep: Detiene el proceso durante el tiempo especificado
//*********************************************************************
void Timer_Sleep(unsigned int time);

//*********************************************************************
// Delay: Delay por software
//*********************************************************************
void Delay(unsigned int n);

#endif