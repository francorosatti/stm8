/**********************************************************************
* Name: spi.h
* Desc: Header de la libreria spi.c
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Aug-2017
**********************************************************************/

#ifndef SPI_H
#define SPI_H

#define SPI_ENABLED             SPI_CR1 |= MASK_SPI_CR1_SPE
#define SPI_DISABLED            SPI_CR1 &= (~MASK_SPI_CR1_SPE)
#define CUENTAS_TO_TEMP(th, tl) (unsigned int)(31.25f*((float)(((th << 8) + tl) >> 2)))
//*********************************************************************
// SPI_Init: Inicializa el SPI
//*********************************************************************
void SPI_Init(void);

//*********************************************************************
// SPI_Recv: Recibe un dato por SPI
//*********************************************************************
unsigned int SPI_Recv(void);

#endif