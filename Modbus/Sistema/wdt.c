/**********************************************************************
* Name: wdt.c
* Desc: Libreria para el manejo del Watchdog Timer
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Aug-2017
**********************************************************************/

#include "wdt.h"
#include "iostm8s003k3.h"

#ifndef WDT_DISABLE
#define WDT_DISABLE 0
#endif

//*********************************************************************
// Wdt_Init: Inicia el Watchdog Timer
//
// Se generar� un pulso de reset cuando el contador llegue a cero
//
// Prescaler:   000 (/4), 001 (/8), 010 (/16), 011 (/32)
//              100 (/64), 101 (/128), 110 (/256), 111 (reservado)
//
// Timeout = 2*TLSI*P*Reload = (2*(2^8)*255)/128kHz = 1.02 seg
// TLSI    = 1/128kHz
// P       = 2^(Presc + 2)
//*********************************************************************
void Wdt_Init(){
#if (!WDT_DISABLE)
  IWDG_KR  = 0x55;
  IWDG_PR  = 0x06;
  IWDG_RLR = 0xFF;
  IWDG_KR  = 0xCC;
#endif
}

//*********************************************************************
// Wdt_Reset: Resetea el Watchdog Timer
//*********************************************************************
void Wdt_Reset(){
#if (!WDT_DISABLE)
  IWDG_KR  = 0xAA;
#endif
}