/**********************************************************************
* Name: timer.c
* Desc: Libreria para el manejo del Timer
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Aug-2017
**********************************************************************/

#include "timer.h"
#include "wdt.h"
#include "iostm8s003k3.h"

static unsigned int TimerTickCount = 0;
static unsigned int TimerSecdCount = 0;
static unsigned int TimerDvsrCount = 0;

//*********************************************************************
// Timer_Init: Inicializa el Timer 2
//
//  ARR = ______fmaster_______
//          PSCR * ftimebase
//  fmaster = 2MHz
//  ftimebase = 1kHz
//  PSCR = 1
//
//  ARR = 2000dec (7D0h)
//*********************************************************************
void Timer_Init()
{
  TIM2_PSCR = 0x00; //Prescaler en 1;
  TIM2_ARRH = 0x07; //Auto-reload Register High
  TIM2_ARRL = 0xD0; //Auto-reload Register Low
  TIM2_IER = 0x01; //Habilito la interrupcion por Update
  TIM2_CR1 = 0x01; //Habilito el timer
}

//*********************************************************************
// Timer_GetTickCount: Obtiene la cuenta actual del Timer 2 en ms
//*********************************************************************
unsigned int Timer_GetTickCount(){
  return TimerTickCount;
}

//*********************************************************************
// Timer_GetSecdCount: Obtiene la cuenta actual del Timer 2 en segundos
//*********************************************************************
unsigned int Timer_GetSecdCount(){	
  return TimerSecdCount;
}

//*********************************************************************
// Timer_GetCounter: Obtiene el valor del contador de hardware
//*********************************************************************
unsigned short Timer_GetCounter(){
  unsigned short h = ((unsigned short)TIM2_CNTRH)<<8;
  return (h + (unsigned short)TIM2_CNTRL);
}

//*********************************************************************
// Timer_Sleep: Detiene el proceso durante el tiempo especificado
//
// Parámetros:
//      t: tiempo en milisegundos que deseo detener el proceso
//*********************************************************************
void Timer_Sleep(unsigned int t){
  unsigned int tickinicial = TimerTickCount;
  while(TimerTickCount - tickinicial < t)
    Wdt_Reset();
}

//*********************************************************************
// Delay: Detiene el proceso durante un cierta cantidad de ciclos
//
// Parámetros:
//      n: Cantidad de ciclos de for que se detendrá el proceso
//*********************************************************************
void Delay(unsigned int n){
  for (;n;n--)
    Wdt_Reset();
}

//*********************************************************************
// Timer2_OverflowHandler: Maneja la interrupción del Timer 2
//
// La interrupción se genera cada 1ms
//*********************************************************************
#pragma vector = TIM2_OVR_UIF_vector
__interrupt void Timer2_OverflowHandler(){
  //Bajo el flag de la interrupción del timer
  TIM2_SR1 &= (~MASK_TIM2_SR1_UIF);
  //Aqui entra cada 1ms
  TimerTickCount++;
  TimerDvsrCount++;
  if(TimerDvsrCount >= TICKS_POR_SEGUNDO){
    TimerDvsrCount = 0;
    TimerSecdCount++;
  }
}