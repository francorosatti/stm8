/**********************************************************************
* Name: ports.c
* Desc: Libreria para la configuración de los puertos del mcu
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Aug-2017
**********************************************************************/

#include "ports.h"
#include "iostm8s003k3.h"

//*********************************************************************
// Ports_Init: Inicializa los puertos del mcu
//*********************************************************************
void Ports_Init(){
  // LED
  PD_DDR_bit.DDR0 = 1; //Output
  PD_CR1_bit.C10 = 1; //Push-pull
  PD_CR2_bit.C20 = 1; //High Speed
  //RE
  PD_DDR_bit.DDR2 = 1; //Output
  PD_CR1_bit.C12 = 1; //Push-pull
  PD_CR2_bit.C22 = 1; //High Speed
  //DE
  PD_DDR_bit.DDR3 = 1; //Output
  PD_CR1_bit.C13 = 1; //Push-pull
  PD_CR2_bit.C23 = 1; //High Speed
  // CS
  PB_DDR_bit.DDR7 = 1; //Output
  PB_CR1_bit.C17 = 1; //Push-pull
  PB_CR2_bit.C27 = 0; //Slow Speed
  //SPI output pins must be set-up as push-pull, fast slope for optimal operation.
  //MOSI
  PC_DDR_bit.DDR6 = 1; //Output
  PC_CR1_bit.C16 = 1; //Push-pull
  PC_CR2_bit.C26 = 1; //High Speed
  //SCK
  PC_DDR_bit.DDR5 = 1; //Output
  PC_CR1_bit.C15 = 1; //Push-pull
  PC_CR2_bit.C25 = 1; //High Speed 
}