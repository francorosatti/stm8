/**********************************************************************
* Name: modbus.h
* Desc: Header de la libreria modbus.c
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Aug-2017
**********************************************************************/

#ifndef MODBUS_H
#define MODBUS_H

#define	u16(Buffer, Offset)     (*(unsigned short*)&Buffer[Offset])
#define	MB_RW_OK        0
#define	MB_RW_ERROR     1
#define	MB_ADDR_ERROR   2

//********************************************************************************
// Headers
//********************************************************************************
//Modbus Serie
#define MBRTUH_SLAVE		0
#define MBRTUH_SLAVE_SIZE       1
#define MBRTUH_CHK_SIZE		2

//Modbus
#define MBP_FUNCION     0
#define MBP_DATA0	1
#define MBP_DATA1	2
#define MBP_DATA2	3
#define MBP_DATA3	4
#define MBP_DATA4	5
#define MBP_DATA5	6

//********************************************************************************
// C�digo de funciones
//********************************************************************************
#define MBF_READ_COIL				1 //No se implementa
#define MBF_READ_INPUT_STATUS			2 //No se implementa
#define MBF_READ_HOLDING_REGISTERS		3 //Read Analog OutPuts;
#define MBF_READ_INPUT_REGISTERS		4 //Read Analog InPuts;
#define MBF_FORCE_SINGLE_COIL			5 //No se implementa
#define MBF_PRESET_SINGLE_REGISTER		6 //No se implementa
#define MBF_READ_EXCEPTION_STATUS		7 //No se implementa
#define MBF_LOOPBACK_TEST			8 //No se implementa
#define MBF_PROGRAM				9 //No se implementa
#define MBF_POLL_PROGRAM_COMPLETE		10 //No se implementa
#define MBF_FETCH_EVENT_COUNTER_COMMUNICATIONS  11 //No se implementa
#define MBF_FETCH_COMMUNICATIONS_EVENT_LOG	12 //No se implementa
#define MBF_FORCE_MULTIPLE_COILS		15 //No se implementa
#define MBF_PRESET_MULTIPLE_REGISTERS		16 //No se implementa
#define MBF_REPORT_SLAVE_ID			17 //No se implementa

//********************************************************************************
// C�digo de exepciones
//********************************************************************************
#define MBE_ILLEGAL_FUNCTION		1
#define MBE_ILLEGAL_DATA_ADDRESS        2
#define MBE_ILLEGAL_DATA_VALUE		3
#define MBE_DEVICE_FAILURE              4

//********************************************************************************
// MB_Procesar: Procesa un paquete modbus
//********************************************************************************
int MB_Exception(unsigned char* TxBuffer, unsigned char Excep);

//********************************************************************************
// MBSProcesar: Procesa un paquete modbus
//********************************************************************************
int MB_Procesar(unsigned char* TxBuffer, unsigned char* RxBuffer, int RxLen);

//********************************************************************************
// MB_ProcesarRTU: Procesa un paquete modbus RTU
//********************************************************************************
int MB_ProcesarRTU(unsigned char* TxBuffer, unsigned char* RxBuffer, int RxLen);

//********************************************************************************
// MB_ReadAnalogOutput: CallBack de lectura de una salida anal�gica
//********************************************************************************
int MB_ReadAnalogOutput(unsigned int Addr, unsigned int* Value);

//********************************************************************************
// MB_ReadAnalogInput: CallBack de lectura de una entrada anal�gica
//********************************************************************************
int MB_ReadAnalogInput(unsigned int Addr, unsigned int* Value);

//********************************************************************************
// MB_Motor: Motor de la comunicaci�n por protocolo Modbus
//********************************************************************************
void MB_Motor(void);

#endif