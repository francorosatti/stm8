/**********************************************************************
* Name: spi.c
* Desc: Libreria para la comunición por SPI
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Aug-2017
**********************************************************************/

#include "spi.h"
#include "ports.h"
#include "system.h"
#include "iostm8s003k3.h"

//*********************************************************************
// SPI_Init: Inicializa el SPI
//*********************************************************************
void SPI_Init(){
    SPI_CS_DISABLED;
    //CLK_PCKENR1 |= 0x2; //Enable SPI peripheral clock
    SPI_CR1 = MASK_SPI_CR1_BR | MASK_SPI_CR1_MSTR | MASK_SPI_CR1_CPHA; //MSB First,BaudRate: fmaster/256 Slowest,SCK Idle Low, Falling Edge, Master Mode
    SPI_CR2 = MASK_SPI_CR2_RXONLY | MASK_SPI_CR2_SSM | MASK_SPI_CR2_SSI; //Input, managed by software, RXOnly,CRC Disabled
}

//*********************************************************************
// SPI_Recv: Recibe un dato por SPI
//*********************************************************************
short SPI_Recv(){
  unsigned short temp_high = 0;
  unsigned short temp_low = 0;
  IRQ_DISABLED;
  SPI_CS_ENABLED;
  SPI_DR = 0x00;
  SPI_ENABLED;
  while(!SPI_SR_RXNE){} //Wait for RxFlag
  temp_high = (unsigned int)SPI_DR; //Read RX Buffer
  while(!SPI_SR_RXNE){} //Wait for RxFlag
  temp_low = (unsigned int)SPI_DR; //Read RX Buffer
  SPI_CS_DISABLED;
  SPI_DISABLED;
  IRQ_ENABLED;
  return (short)(((temp_high << 8) + temp_low) >> 2);
}