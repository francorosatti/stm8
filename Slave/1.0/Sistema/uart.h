/**********************************************************************
* Name: uart.h
* Desc: Header de la libreria uart.c
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Aug-2017
**********************************************************************/

#ifndef UART_H
#define UART_H

//*********************************************************************
// Defines
//*********************************************************************

// Tama�o del buffer de recepci�n
#define UART_RX_BUFFER_LEN 	32
#define UART_RX_BUFFER_MASK 	0x1F

// Tama�o del buffer de Transmision
#define UART_TX_BUFFER_LEN 	32
#define UART_TX_BUFFER_MASK     0x1F

//Define el tiempo de silencio entre paquetes ModBus en MiliSegundos
#define UART_SILENCE_TIME 	10

//Define el tiempo de silencio entre bytes ModBus en MiliSegundos
#define UART_BYTE_TIME 		4

//Guarda el tiempo actual
#define UART_SAVE_TIME          Uart_TickRef = Timer_GetTickCount()

//Estados posibles de la m�quina de estados de la UART
#define UART_INICIANDO          0
#define UART_ESCUCHANDO         1
#define UART_RECIBIENDO         2
#define UART_TRANSMITIENDO      3
#define UART_FIN_TRANSMISION    4

//*********************************************************************
// UART_Init: Inicia la UART
//*********************************************************************
void Uart_Init(void);

//*********************************************************************
// Uart_Motor: Motor de la comunicaci�n serie
//*********************************************************************
void Uart_Motor(void);

#endif