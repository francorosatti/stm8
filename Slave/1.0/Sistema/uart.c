/**********************************************************************
* Name: uart.c
* Desc: Libreria para la comunición por puerto serie
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Aug-2017
**********************************************************************/

#include "system.h"
#include "timer.h"
#include "ports.h"
#include "uart.h"
#include "modbus.h"
#include "iostm8s003k3.h"

//*********************************************************************
// Variables Globales
//*********************************************************************
static unsigned char UartRxBuffer[UART_RX_BUFFER_LEN];
static unsigned char UartTxBuffer[UART_TX_BUFFER_LEN];

static unsigned int UartRxIndex = 0;
static unsigned int UartTxInIndex = 0;
static unsigned int UartTxOutIndex = 0;

static unsigned char Uart_Estado = UART_INICIANDO;
static unsigned int Uart_TickRef = 0;
//*********************************************************************
// Uart_Init: Inicia la UART
//*********************************************************************
void Uart_Init(){
  UART1_BRR2 = 0x00;
  UART1_BRR1 = 0x0D;
  UART1_CR1 = 0x00;
  UART1_CR3 = 0x00;
  UART1_CR4 = 0x00;
  UART1_CR5 = 0x00;
  UART1_GTR = 0x00;
  UART1_PSCR = 0x00;
  
  UartRxIndex = 0;
  RS485_TX_DISABLED;
  RS485_RX_ENABLED;
  Uart_Estado = UART_ESCUCHANDO;
  UART1_CR2 = MASK_UART1_CR2_REN | MASK_UART1_CR2_RIEN;
  
  UART_SAVE_TIME;
}

//*********************************************************************
// Uart_ReceiveHandler: Maneja la interrupción por recepción
//
// La interrupcón se genera cuando hay un dato nuevo en DR
//*********************************************************************
#pragma vector = UART1_R_RXNE_vector
__interrupt void Uart_RxBufferNotEmptyHandler(){
  if(UART1_SR & MASK_UART1_SR_RXNE){
    if(Uart_Estado == UART_TRANSMITIENDO) return;
    if(Uart_Estado == UART_ESCUCHANDO) Uart_Estado = UART_RECIBIENDO;
    if(Uart_Estado == UART_RECIBIENDO && UartRxIndex < UART_RX_BUFFER_LEN){      
      UartRxBuffer[UartRxIndex & UART_RX_BUFFER_MASK] = UART1_DR;
      UartRxIndex++;
    }
    UART_SAVE_TIME;
  }
}

//*********************************************************************
// Uart_TransmissionCompleteHandler: 
//      Maneja la interrupción por transmisión completa
//
// La interrupcón se genera cuando la transmisión de un dato se ha
// completado
//*********************************************************************
#pragma vector = UART1_T_TXE_vector
__interrupt void Uart_TxBufferEmptyHandler(){
  unsigned char sr = UART1_SR;
  if(sr & MASK_UART1_SR_TC){
    //Bajo el flag de Transmission Buffer Empty
    UART1_SR &= (~MASK_UART1_SR_TC);
    if(Uart_Estado == UART_RECIBIENDO) return;
    if(Uart_Estado == UART_ESCUCHANDO) Uart_Estado = UART_TRANSMITIENDO;
    if(Uart_Estado == UART_TRANSMITIENDO && UartTxOutIndex < UART_TX_BUFFER_LEN){
      if(UartTxOutIndex >= UartTxInIndex){
        Uart_Estado = UART_FIN_TRANSMISION;
        return;
      }else{
        UART1_DR = UartTxBuffer[UartTxOutIndex & UART_TX_BUFFER_MASK];
        UartTxOutIndex++;
      }
    }
    UART_SAVE_TIME;
  }
}

//********************************************************************************
// Uart_Motor: Motor de la comunicación serie
//********************************************************************************
void Uart_Motor(){
  IRQ_DISABLED;
  
  //Pregunto si estoy recibiendo datos
  if(Uart_Estado == UART_RECIBIENDO){
    //Pregunto si ya paso el tiempo para considerar fin de la recepción de datos
    if(Timer_GetTickCount() - Uart_TickRef > 10*UART_BYTE_TIME){
      UartTxInIndex = MB_ProcesarRTU(UartTxBuffer,UartRxBuffer,UartRxIndex);
      if(UartTxInIndex > 0){
        UartTxOutIndex = 0;
        RS485_RX_DISABLED;
        RS485_TX_ENABLED;
        Uart_Estado = UART_TRANSMITIENDO;
        UART1_CR2 = MASK_UART1_CR2_TEN | MASK_UART1_CR2_TCIEN;
        UART1_DR = UartTxBuffer[UartTxOutIndex & UART_TX_BUFFER_MASK];
        UartTxOutIndex++;
      }else{
        UartRxIndex = 0;
        RS485_TX_DISABLED;
        RS485_RX_ENABLED;
        Uart_Estado = UART_ESCUCHANDO;
        UART1_CR2 = MASK_UART1_CR2_REN | MASK_UART1_CR2_RIEN;
      }
    }
  }
  //Pregunto si se terminaron de transmitir los datos
  if(Uart_Estado == UART_FIN_TRANSMISION){
    UartRxIndex = 0;
    RS485_TX_DISABLED;
    RS485_RX_ENABLED;
    Uart_Estado = UART_ESCUCHANDO;
    UART1_CR2 = MASK_UART1_CR2_REN | MASK_UART1_CR2_RIEN;
  }
  
  IRQ_ENABLED;
}