/**********************************************************************
* Name: system.h
* Desc: Header con definiciones comunes a todo el sistema
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Aug-2017
**********************************************************************/

#ifndef SYSTEM_H
#define SYSTEM_H

#define IRQ_DISABLED    asm("sim")
#define IRQ_ENABLED     asm("rim")

#define MEM_PROTECTION_KEY      0x9B
#define MEM_PROTECTION_NKEY      0x64

//*********************************************************************
// Reset: Resetea el micro
//*********************************************************************
void Reset(void);

void EnableMemoryProtection(void);
#endif