/****************************************************
* Name: main.c (Numeracion)
* Desc: Testeo de la numeración de los esclavos
* Vers: 0.1
* Date: Oct-2017
* Auth: Franco Rosatti
****************************************************/

//****************************************************
// Includes
//****************************************************
#include "system.h"
#include "clock.h"
#include "wdt.h"
#include "timer.h"
#include "ports.h"
#include "uart.h"
#include "spi.h"
#include "eeprom.h"
#include "iostm8s003k3.h"

//****************************************************
// Defines
//****************************************************
#define LED_INIT_TIMEOUT      3000
#define LED_TGL_TIMEOUT       250
#define TEMP_MED_TIMEOUT      250
#define MAX_TEMP              0x4B03

//****************************************************
// Variables Globales
//****************************************************
static unsigned int led_tickref;
static unsigned int spi_tickref;
short Temperatura = MAX_TEMP;
extern unsigned char memory_key;


//****************************************************
// Main
//****************************************************
int main(void){
  //Protejo la memoria
  //EnableMemoryProtection();
  
  //Inicializacion de Perifericos
  Clock_Init();
  Timer_Init();
  Ports_Init();
  Uart_Init();
  SPI_Init();
  Wdt_Init();

  //Inicializacion Contadores
  led_tickref = Timer_GetTickCount();
  spi_tickref = Timer_GetTickCount();
  
  //Inicio
  memory_key = MEM_PROTECTION_NKEY;
  IRQ_ENABLED;
      
  //Secuencia LED Inicio
  LED_ON;
  while (1){
    Wdt_Reset();
    if(Timer_GetTickCount() - led_tickref >= LED_INIT_TIMEOUT){
      led_tickref += LED_INIT_TIMEOUT;
      break;
    }
  }
  LED_OFF;
  
  //Lazo principal
  while (1){
    //Reseteo el WatchDog Timer
    Wdt_Reset();
    
    //Toogle Led
    if(Timer_GetTickCount() - led_tickref >= LED_TGL_TIMEOUT){
      led_tickref += LED_TGL_TIMEOUT;
      LED_TGL;
    }

    //Medicion de Temperatura
    if(Timer_GetTickCount() - spi_tickref >= TEMP_MED_TIMEOUT){
      spi_tickref += TEMP_MED_TIMEOUT;
      Temperatura = SPI_Recv();
    }
    
    //Proceso del puerto serie
    Uart_Motor();
  }
}