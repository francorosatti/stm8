/**********************************************************************
* Name: modbus.c
* Desc: Libreria para la comunicaci�n por protocolo Modbus
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Aug-2017
**********************************************************************/

#include "system.h"
#include "eeprom.h"
#include "modbus.h"

#pragma location = ".cfg"
unsigned char MB_SId = 1;
#pragma location = ".fct"
char SN[11] = "RBTSLV00002";

extern unsigned int Temperatura;
extern unsigned int ADC_Data;
extern unsigned char memory_key;

/*
// ***************************************************************************************************************************************
// Tabla CRC High: tabla de valores para la parte alta del CRC de Modbus RTU
// ***************************************************************************************************************************************
  const unsigned char TablaCRCHigh[] = {0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
                                        0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
                                        0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
                                        0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
                                        0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
                                        0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
                                        0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
                                        0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
                                        0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
                                        0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
                                        0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
                                        0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
                                        0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
                                        0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
                                        0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
                                        0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40};

// ***************************************************************************************************************************************
// Tabla CRC Low: tabla de valores para la parte baja del CRC de Modbus RTU
// ***************************************************************************************************************************************
  const unsigned char TablaCRCLow[] = {0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6, 0x06, 0x07, 0xC7, 0x05, 0xC5, 0xC4, 0x04,
                                       0xCC, 0x0C, 0x0D, 0xCD, 0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, 0x0B, 0xC9, 0x09, 0x08, 0xC8,
                                       0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A, 0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC,
                                       0x14, 0xD4, 0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3, 0x11, 0xD1, 0xD0, 0x10,
                                       0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3, 0xF2, 0x32, 0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4,
                                       0x3C, 0xFC, 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A, 0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38,
                                       0x28, 0xE8, 0xE9, 0x29, 0xEB, 0x2B, 0x2A, 0xEA, 0xEE, 0x2E, 0x2F, 0xEF, 0x2D, 0xED, 0xEC, 0x2C,
                                       0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26, 0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, 0xE0,
                                       0xA0, 0x60, 0x61, 0xA1, 0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67, 0xA5, 0x65, 0x64, 0xA4,
                                       0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F, 0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68,
                                       0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA, 0xBE, 0x7E, 0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C,
                                       0xB4, 0x74, 0x75, 0xB5, 0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71, 0x70, 0xB0,
                                       0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92, 0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54,
                                       0x9C, 0x5C, 0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B, 0x99, 0x59, 0x58, 0x98,
                                       0x88, 0x48, 0x49, 0x89, 0x4B, 0x8B, 0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
                                       0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42, 0x43, 0x83, 0x41, 0x81, 0x80, 0x40};

// ********************************************************************************
// MBSCheckSum: Calcula el CheckSum de un paquete Modbus Serie
// ********************************************************************************
unsigned short MBSCheckSum(unsigned char* Buffer, unsigned char Len){
  unsigned char i;
  unsigned char CRCHigh = 0xff;
  unsigned char CRCLow = 0xff;
  unsigned char Index;
  for(i=0; i<Len; i++){
    Index = (unsigned char) (CRCHigh ^ Buffer[i]);
    CRCHigh = (unsigned char) (CRCLow ^ TablaCRCHigh[Index]);
    CRCLow = TablaCRCLow[Index];
  }
  return (unsigned short) (CRCHigh << 8 | CRCLow);
}
*/

//********************************************************************************
// MB_CheckSum: Calcula el CheckSum de un paquete Modbus Serie
//********************************************************************************
static unsigned short MB_CheckSum(unsigned char* Buffer, unsigned char Len){
  int pos, i;
  unsigned short crc = 0xFFFF;
	
  for (pos=0; pos<Len; pos++){
    crc ^= (unsigned short)Buffer[pos];
		
    for (i=8; i!=0; i--){
      if ((crc & 0x0001)!=0){
        crc >>= 1;
	crc ^= 0xA001;
      }else{
        crc >>= 1;
      }
    }
  }
  return (unsigned short)(((crc & 0xff00) >> 8) | ((crc & 0x00ff) << 8));
}

//********************************************************************************
// MB_ProcesarRTU: Procesa un paquete modbus de comunicac�n serial
//
// Par�metros:
//	TxBuffer: Puntero al buffer de Transmisi�n, para encolar la respuesta
//	RxBuffer: Puntero al buffer de Recepci�n, donde se encuentra la pregunta
//	RxLen:    Cantidad de datos en el buffer de Recepcci�n.
//
// Retorno:
//      Cantidad de datos encolados en el Buffer de Transmisi�n
//********************************************************************************
int MB_ProcesarRTU(unsigned char* TxBuffer, unsigned char* RxBuffer, int RxLen){
  int TxLen;
  
  //Chequear el Identificador de Esclavo
  if(RxBuffer[MBRTUH_SLAVE] == MB_SId){
    //Paquete dirigido a este esclavo
    RxLen -= MBRTUH_CHK_SIZE;

    //Chequear el Largo del paquete
    if(RxLen < MBRTUH_SLAVE_SIZE) return 0;

    //Chequear el CheckSum
    if(u16(RxBuffer, RxLen) != MB_CheckSum(RxBuffer, (unsigned char)RxLen)) return 0;

    //Procesar el Pedido
    TxLen = MB_Procesar(TxBuffer + MBRTUH_SLAVE_SIZE, RxBuffer + MBRTUH_SLAVE_SIZE, RxLen - MBRTUH_SLAVE_SIZE);

    //Enviar la Respuesta
    if(TxLen <= 0) return 0;

    //Genero la Respuesta
    TxBuffer[MBRTUH_SLAVE] = MB_SId;
    TxLen += MBRTUH_SLAVE_SIZE;
    u16(TxBuffer, TxLen) = MB_CheckSum(TxBuffer, (unsigned char)TxLen);

    //Retorno la Cantidad de datos a enviar
    return TxLen + MBRTUH_CHK_SIZE;
  }else if(RxBuffer[MBRTUH_SLAVE] == MBRTU_BRDCST_ADDR){
    //Paquete BroadCast
    RxLen -= MBRTUH_CHK_SIZE;

    //Chequear el Largo del paquete
    if(RxLen < MBRTUH_SLAVE_SIZE) return 0;

    //Chequear el CheckSum
    if(u16(RxBuffer, RxLen) != MB_CheckSum(RxBuffer, (unsigned char)RxLen)) return 0;

    //Chequear Tama�o del paquete broadcast
    if(RxLen - MBRTUH_SLAVE_SIZE < MBRTU_BRDCST_SIZE) return 0;
    
    //Chequear que el numero de serie recibido sea el propio
    for(TxLen = 0; TxLen < 11; TxLen++){
      if(RxBuffer[MBRTUH_SLAVE_SIZE + TxLen] != SN[TxLen]){
        return 0;
      }else{
        TxBuffer[MBRTUH_SLAVE_SIZE + TxLen] = RxBuffer[MBRTUH_SLAVE_SIZE + TxLen];
      }
    }
        
    //Chequear que sea una direcci�n modbus v�lida
    if(RxBuffer[MBRTUH_SLAVE_SIZE + TxLen] < MBRTU_MIN_ADDR || RxBuffer[MBRTUH_SLAVE_SIZE + TxLen] > MBRTU_MAX_ADDR) return 0;
    //Guardar la direcci�n modbus recibida
    memory_key = ~memory_key;
    Eeprom_Write(&MB_SId,RxBuffer[MBRTUH_SLAVE_SIZE + TxLen]);
    memory_key = ~memory_key;
  
    //Completar la respuesta
    TxBuffer[MBRTUH_SLAVE_SIZE + TxLen] = MB_SId;
    TxLen++;
    
    //Enviar la Respuesta
    if(TxLen <= 0) return 0;

    //Genero la Respuesta
    TxBuffer[MBRTUH_SLAVE] = MBRTU_BRDCST_ADDR;
    TxLen += MBRTUH_SLAVE_SIZE;
    u16(TxBuffer, TxLen) = MB_CheckSum(TxBuffer, (unsigned char)TxLen);

    //Retorno la Cantidad de datos a enviar
    return TxLen + MBRTUH_CHK_SIZE; 
  }else {
    //Paquete dirigido a otro esclavo
    return 0;
  }
}

//********************************************************************************
// MB_Exception: Procesa la exepcion de un paquete modbus
//********************************************************************************
int MB_Exception(unsigned char* TxBuffer, unsigned char Excep){
  // Activa el bit 8 del codigo de funcion para indicar que es una excepcion
  TxBuffer[MBP_FUNCION] |= 0x80;
  // Excepcion del paquete
  TxBuffer[MBP_DATA0] = Excep;
  // Retorna el largo de la exepci�n
  return 2;
}

//********************************************************************************
// MB_Procesar: Procesa un paquete modbus
//
// Retorno:
//      Cantidad de bytes encolados para transmisi�n
//********************************************************************************
int MB_Procesar(unsigned char* TxBuffer, unsigned char* RxBuffer, int RxLen){
  TxBuffer[MBP_FUNCION] = RxBuffer[MBP_FUNCION];

  //Read Analog OutPuts
  if(RxBuffer[MBP_FUNCION] ==  MBF_READ_HOLDING_REGISTERS){
    unsigned int Dir, Cant, Val, *Regs;
    unsigned char Bytes;
    int Ret;
			
    //Obtengo la Direcci�n Inicial
    Dir = u16(RxBuffer, MBP_DATA0);

    //Obtengo la Cantidad de Registros a Leer
    Cant = u16(RxBuffer, MBP_DATA2);

    //Chequeo la Consistencia del Paquete
    if(RxLen != 5 || Cant == 0 || Cant > 13) return MB_Exception(TxBuffer, MBE_ILLEGAL_DATA_VALUE);

    //Leo las Salidas Anal�gicas
    Regs = (unsigned int*)(TxBuffer + MBP_DATA1);
    Bytes = (unsigned char)(Cant*sizeof(int));
    for(; Cant != 0; Cant--, Dir++, Regs++){
      Ret = MB_ReadAnalogOutput(Dir, &Val);
      if (Ret != MB_RW_OK){
        if (Ret == MB_RW_ERROR) return MB_Exception(TxBuffer, MBE_DEVICE_FAILURE);
        if (Ret == MB_ADDR_ERROR) return MB_Exception(TxBuffer, MBE_ILLEGAL_DATA_ADDRESS);
      }
      *Regs = Val;
    }
    TxBuffer[MBP_DATA0] = Bytes;
    return (Bytes + 2);
  }

  //Read Analog Inputs
  if(RxBuffer[MBP_FUNCION] == MBF_READ_INPUT_REGISTERS){
    unsigned int Dir, Cant, Val, *Regs;
    unsigned char Bytes;
    int Ret;
			
    //Obtengo la Direcci�n Inicial
    Dir = u16(RxBuffer, MBP_DATA0);

    //Obtengo la Cantidad de Registros a Leer
    Cant = u16(RxBuffer, MBP_DATA2);

    //Chequeo la Consistencia del Paquete
    if(RxLen != 5 || Cant == 0 || Cant > 125) return MB_Exception(TxBuffer, MBE_ILLEGAL_DATA_VALUE);

    //Leo las Entradas Anal�gicas
    Regs = (unsigned int*)(TxBuffer + MBP_DATA1);
    Bytes = (unsigned char)(Cant*sizeof(int));
    for (; Cant != 0; Cant--, Dir++, Regs++){
      Ret = MB_ReadAnalogInput(Dir, &Val);
      if(Ret != MB_RW_OK){
        if(Ret == MB_RW_ERROR) return MB_Exception(TxBuffer, MBE_DEVICE_FAILURE);
	if(Ret == MB_ADDR_ERROR) return MB_Exception(TxBuffer, MBE_ILLEGAL_DATA_ADDRESS);
      }
      *Regs = Val;
    }
    TxBuffer[MBP_DATA0] = Bytes;
    return (Bytes + 2);
  }
	
  //Retorno la Ecepci�n de Error de Funcion
  return MB_Exception(TxBuffer, MBE_ILLEGAL_FUNCTION);
}

//********************************************************************************
// MB_ReadAnalogOutput: CallBack de lectura de una salida anal�gica
//
// Par�metros:
//	Addr: Direcci�n de la salida anal�gica
//	Value: Puntero a la posici�n de memoria donde se debe dejar el valor
//
// Retorno:
// 	MB_RW_OK:	Sin Problemas
//	MB_RW_ERROR:	Error de Lectura
//	MB_ADDR_ERROR:	Error de Direcci�n
//********************************************************************************
int MB_ReadAnalogOutput(unsigned int Addr, unsigned int* Value){
  if(Addr == 0){
    *Value = Temperatura;
  }else{
    *Value = 0;
  }
  return MB_RW_OK;
}

//********************************************************************************
// MB_ReadAnalogInput: CallBack de lectura de una entrada anal�gica
//
// Par�metros:
//      Addr: Direcci�n de la entrada anal�gica
//      Value: Puntero a la posici�n de memoria donde se debe dejar el valor
//
// Retorno:
//      MB_RW_OK:	Sin Problemas
//      MB_RW_ERROR:	Error de Lectura
//      MB_ADDR_ERROR:	Error de Direcci�n
//********************************************************************************
int MB_ReadAnalogInput(unsigned int Addr, unsigned int* Value){
  if(Addr == 0){
    *Value = Temperatura;
  }else{
    *Value = 0;
  }
  return MB_RW_OK;
}