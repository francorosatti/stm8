/****************************************************
* Name: main.c (Numeracion)
* Desc: Testeo de la numeración de los esclavos
* Vers: 0.1
* Date: Oct-2017
* Auth: Franco Rosatti
****************************************************/

#include "system.h"
#include "clock.h"
#include "wdt.h"
#include "timer.h"
#include "ports.h"
#include "uart.h"
#include "spi.h"
#include "adc.h"
#include "flash.h"
#include "eeprom.h"
#include "iostm8s003k3.h"

#define LED_INIT_TIMEOUT      3000
#define LED_TGL_TIMEOUT       250
#define TEMP_MED_TIMEOUT      250
#define ADC_MED_TIMEOUT       50

static unsigned int led_tickref;
static unsigned int spi_tickref;
static unsigned int adc_tickref;
unsigned int Temperatura = 0;
extern unsigned int ADC_Data;
extern unsigned int ADC_Buf[20];
extern unsigned char memory_key;

int main(void){
  //Variables
  int i = 0;
  int acc = 0;
  
  //Protejo la memoria
  //EnableMemoryProtection();
  
  //Inicializacion de Perifericos
  Clock_Init();
  Timer_Init();
  Ports_Init();
  Uart_Init();
  SPI_Init();
  ADC_Init();
  Wdt_Init();

  //Inicializacion Contadores
  led_tickref = Timer_GetTickCount();
  spi_tickref = Timer_GetTickCount();
  adc_tickref = Timer_GetTickCount();
  
  //Inicio
  memory_key = MEM_PROTECTION_NKEY;
  ADC_ON;
  LED_OFF;
  IRQ_ENABLED;
      
  //Secuencia LED Inicio
  LED_ON;
  while (1){
    Wdt_Reset();
    if(Timer_GetTickCount() - led_tickref >= LED_INIT_TIMEOUT){
      led_tickref += LED_INIT_TIMEOUT;
      break;
    }
  }
  LED_OFF;
  
  while (1){
    //Reseteo el WatchDog Timer
    Wdt_Reset();
    
    //Toogle Led
    if(Timer_GetTickCount() - led_tickref >= LED_TGL_TIMEOUT){
      led_tickref += LED_TGL_TIMEOUT;
      LED_TGL;
    }

    //Medicion de Temperatura
    if(Timer_GetTickCount() - spi_tickref >= TEMP_MED_TIMEOUT){
      spi_tickref += TEMP_MED_TIMEOUT;
      Temperatura = SPI_Recv();
    }

    //Medicion del ADC
    if(Timer_GetTickCount() - adc_tickref >= ADC_MED_TIMEOUT){
      adc_tickref += ADC_MED_TIMEOUT;
      ADC_StartConvertion();
    }
    
    //Proceso del puerto serie
    Uart_Motor();
    
    //Filtro pasa bajos
    acc = 0;
    for(i = 0; i < 20; i++){
      acc += ADC_Buf[i];
    }
    acc /= i;
    ADC_Data = acc;
  }
}