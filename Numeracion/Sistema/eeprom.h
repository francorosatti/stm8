/**********************************************************************
* Name: eeprom.h
* Desc: Header de la libreria eeprom.c
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Oct-2017
**********************************************************************/

#ifndef EEPROM_H
#define EEPROM_H

#define FIRST_EEPROM_KEY    0xAE
#define SECOND_EEPROM_KEY   0x56

//*********************************************************************
// Eeprom_Write: Escribe un buffer en la memoria eeprom
//*********************************************************************
void Eeprom_Write(unsigned char* add, char data);

#endif