/**********************************************************************
* Name: eeprom.c
* Desc: Libreria para el acceso a la memoria eeprom
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Oct-2017
**********************************************************************/

#include "system.h"
#include "eeprom.h"
#include "iostm8s003k3.h"

extern unsigned char memory_key;

//*********************************************************************
// Eeprom_Write: Escribe un buffer en la memoria eeprom
//
// Par�metros:
//      addr: direccion de memoria donde se escribir�
//      buffer: puntero al inicio del buffer que se desea escribir
//      len: longitud del buffer a escribir
//*********************************************************************
void Eeprom_Write(unsigned char* addr, char data){
  //Deshabilito las interrupciones
  IRQ_DISABLED;
  //Deshabilito la proteccion de la memoria eeprom
  if(FLASH_IAPSR_DUL == 0){
    if(memory_key == MEM_PROTECTION_KEY){
      FLASH_DUKR = FIRST_EEPROM_KEY;
    }
    if(memory_key == MEM_PROTECTION_KEY){
      FLASH_DUKR = SECOND_EEPROM_KEY; 
    }
  }
  if(FLASH_IAPSR & MASK_FLASH_IAPSR_DUL){
    //Escribo la memoria
    *addr = data;
    //Habilito la proteccion de la memoria eeprom
    FLASH_IAPSR_DUL = 0;
    //Habilito las interrupciones
    IRQ_ENABLED;
  }else{
    Reset();
  }
}