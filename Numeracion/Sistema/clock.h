/**********************************************************************
* Name: clock.h
* Desc: Header de la libreria clock.c
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Aug-2017
**********************************************************************/

#ifndef CLOCK_H
#define CLOCK_H

//*********************************************************************
// Clock_Init: Inicializa el clock del mcu
//*********************************************************************
void Clock_Init(void);

#endif