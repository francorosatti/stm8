/**********************************************************************
* Name: ports.h
* Desc: Header de la libreria ports.c
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Aug-2017
**********************************************************************/

#ifndef PORT_H
#define PORT_H

//*********************************************************************
// Defines
//*********************************************************************
#define LED_ON                  PD_ODR_bit.ODR0 = 0
#define LED_OFF                 PD_ODR_bit.ODR0 = 1
#define LED_TGL                 if(PD_ODR_bit.ODR0 == 1)LED_ON; else LED_OFF;
#define RS485_RX_ENABLED        PD_ODR_bit.ODR2 = 0
#define RS485_RX_DISABLED       PD_ODR_bit.ODR2 = 1
#define RS485_TX_ENABLED        PD_ODR_bit.ODR3 = 1
#define RS485_TX_DISABLED       PD_ODR_bit.ODR3 = 0
#define SPI_CS_ENABLED          PB_ODR_bit.ODR7 = 0
#define SPI_CS_DISABLED         PB_ODR_bit.ODR7 = 1

//*********************************************************************
// Ports_Init: Inicializa los puertos del mcu
//*********************************************************************
void Ports_Init(void);

#endif