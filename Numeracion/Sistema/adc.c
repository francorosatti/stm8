/**********************************************************************
* Name: adc.c
* Desc: Libreria para el manejo del ADC
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Aug-2017
**********************************************************************/

#include "iostm8s003k3.h"

//*********************************************************************
// Variables Globales
//*********************************************************************
unsigned int ADC_Data = 0;
unsigned int ADC_Buf[20];
unsigned int adc_idx = 0;

//*********************************************************************
// ADC_Init: Inicializa el ADC
//*********************************************************************
void ADC_Init(){
  ADC_CSR = MASK_ADC_CSR_EOCIE; //EOC interrupt enabled
  ADC_CR2 = MASK_ADC_CR2_ALIGN; //Align = right
  ADC_CR3 = 0x00;
  ADC_CR1 = 0x00; //NO enciendo el ADC  <--- Atencion
}

//*********************************************************************
// ADC_StartConvertion: Dispara una conversión
//*********************************************************************
void ADC_StartConvertion(){
  //El bit ADC_CR1_ADON ya debe estar seteado
  ADC_CR1 |= MASK_ADC_CR1_ADON;
}

//*********************************************************************
// ADC_EndOfConversionHandler: Maneja la interrupción del ADC
//
// La interrupción se genera cuando termina una conversión
//*********************************************************************
#pragma vector = ADC1_EOC_vector
__interrupt void ADC_EndOfConvertionHandler(void){
  /***************************************************
  * Como ADC_CR2_ALIGN = RIGHT debo leer primero
  * el ADC_DRL y luego el ADC_DRH, este ultimo
  * contiene en sus dos bit menos significativos
  * los dos bits mas significativos de la conversion
  ****************************************************/
  ADC_CSR &= (~MASK_ADC_CSR_EOC); //Bajo el flag de End of Convertion
  //Leo el dato
  ADC_Buf[adc_idx] = ADC_DRL;
  ADC_Buf[adc_idx] |= (((unsigned int)ADC_DRH)<<8);
  if(adc_idx == 19){
    adc_idx = 0;
  }else{
    adc_idx++;
  }
}