/**********************************************************************
* Name: clock.c
* Desc: Libreria para la configuracion del clock del sistema
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Aug-2017
**********************************************************************/

#include "clock.h"
#include "iostm8s003k3.h"

//*********************************************************************
// Clock_Init: Inicializa el clock del mcu
//
// Por defecto inicia con el HSI a 16MHz, el hsi prescaler en 8, cpu
// prescaler en 1.
// fmaster = 2MHz y fcpu = fmaster
//
// CLK_CKDIVR = res|res|res| HSIDIV | HSIDIV | CPUDIV | CPUDIV | CPUDIV
// HSIDIV = 00 (/1), 01 (/2), 10 (/4), 11 (/8)
// CPUDIV = 000 (/1), 001 (/2) ... 111 (/128)
// fcpu = fmaster
//*********************************************************************
void Clock_Init(){
  while(CLK_ICKR_bit.HSIRDY != 1){};
  CLK_CKDIVR = 0x18;
  CLK_PCKENR1 = 0x2E; //Enable Timer2,UART and SPI clock
  CLK_PCKENR2 = 0x7B; //Enable Timer2,UART and SPI clock
}