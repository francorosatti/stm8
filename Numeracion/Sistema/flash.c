/**********************************************************************
* Name: flash.c
* Desc: Libreria para el acceso a la memoria flash
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Oct-2017
**********************************************************************/

#include "system.h"
#include "flash.h"
#include "iostm8s003k3.h"

extern unsigned char memory_key;

//*********************************************************************
// Flash_Write: Escribe un buffer en la memoria flash
//
// Par�metros:
//      addr: direccion de memoria donde se escribir�
//      buffer: puntero al inicio del buffer que se desea escribir
//      len: longitud del buffer a escribir
//*********************************************************************
void Flash_Write(char addr, char* buffer, int len){
  //Deshabilito las interrupciones
  IRQ_DISABLED;
  //Deshabilito la proteccion de la memoria flash
  if(FLASH_IAPSR_PUL == 0){
    if(memory_key == MEM_PROTECTION_KEY){
      FLASH_PUKR = FIRST_FLASH_KEY;
    }
    if(memory_key == MEM_PROTECTION_KEY){
      FLASH_PUKR = SECOND_FLASH_KEY; 
    }
  }
  if(FLASH_IAPSR && MASK_FLASH_IAPSR_PUL){
    //Escribo la memoria
    
    //Habilito la proteccion de la memoria flash
    FLASH_IAPSR_PUL = 0;
    //Habilito las interrupciones
    IRQ_ENABLED;
  }else{
    Reset();
  }
}