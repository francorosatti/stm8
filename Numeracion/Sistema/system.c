/**********************************************************************
* Name: system.c
* Desc: Libreria base del sistema
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Oct-2017
**********************************************************************/

#include "system.h"
#include "iostm8s003k3.h"

unsigned char memory_key = 0;

//*********************************************************************
// Reset: Resetea el micro
//*********************************************************************
void Reset(){
  WWDG_CR |= MASK_WWDG_CR_WDGA;
  WWDG_CR &= ~(MASK_WWDG_CR_T6);
}

void EnableMemoryProtection(){
  FLASH_IAPSR_DUL = 0;
  FLASH_IAPSR_PUL = 0;
}