/**********************************************************************
* Name: flash.h
* Desc: Header de la libreria flash.c
* Auth: Franco Rosatti
* Vers: 1.0
* Date: Oct-2017
**********************************************************************/

#ifndef FLASH_H
#define FLASH_H

#define FIRST_FLASH_KEY    0x56
#define SECOND_FLASH_KEY   0xAE

//*********************************************************************
// Flash_Write: Escribe un buffer en la memoria flash
//*********************************************************************
void Flash_Write(char add, char* buffer, int len);

#endif